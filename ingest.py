#!/usr/bin/env python3
import csv
import os
import sys

from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
from elasticsearch.exceptions import TransportError, ConnectionError, SSLError
import click


def get_total_rows(file: str) -> int:
    """Return the number of low, medium or high risk rows in a Nessus report CSV."""
    with open(file, newline='') as csvfile:
        report = csv.DictReader(csvfile)
        return sum(1 for row in report if row['Risk'] != 'None')


def create_index(client: Elasticsearch) -> dict:
    mapping = {
        "mappings": {
            "properties": {
                "ip_address": {"type": "ip"},
                "host_start": {"type": "date"},
                "host_end": {"type": "date"}
            }
        }
    }
    return client.indices.create(index='findings', body=mapping)


def gendata(file: str) -> dict:
    """Generator of findings from a CSV file.

    Yields one CSV finding as an Elasticsearch document for each iteration on the generator.
    """
    with open(file, newline='') as csvfile:
        # Split out the scan target info from the filename
        scan_id = file.split('_')[0].split('/')[1]
        cluster = file.split('_')[1].split('.csv')[0]
        cluster_type = 'grid'

        # Determine if it's a dedicated cluster
        if '.ent.' in cluster:
            cluster_type = 'dedicated'
            host_type = 'dedicated'

        nessusreader = csv.DictReader(csvfile)
        for row in nessusreader:
            # We don't want to index findings with no risk
            if row['Risk'] != 'None':
                if cluster_type == 'grid':
                    host_type = row['Host'].split(cluster)[1].split('-')[1]
                # This is the Elasticsearch document to generate
                yield {
                    "scan_id": scan_id,
                    "cluster": cluster,
                    "cluster_type": cluster_type,
                    "host_type": host_type,
                    "plugin_id": row['Plugin ID'],
                    "cve": row['CVE'],
                    "cvss": row['CVSS'],
                    "risk": row['Risk'],
                    "host": row['Host'],
                    "protocol": row['Protocol'],
                    "port": row['Port'],
                    "name": row['Name'],
                    "synopsis": row['Synopsis'],
                    "description": row['Description'],
                    "solution": row['Solution'],
                    "see_also": row['See Also'],
                    "plugin_output": row['Plugin Output'],
                    "asset_uuid": row['Asset UUID'],
                    "vulnerability_state": row['Vulnerability State'],
                    "ip_address": row['IP Address'],
                    "fqdn": row['FQDN'],
                    "netbios": row['NetBios'],
                    "os": row['OS'],
                    "mac_address": row['MAC Address'],
                    "plugin_family": row['Plugin Family'],
                    "cvss_base_score": row['CVSS Base Score'],
                    "cvss_temporal_score": row['CVSS Temporal Score'],
                    "cvss_temporal_vector": row['CVSS Temporal Vector'],
                    "cvss_vector": row['CVSS Vector'],
                    "cvss3_base_score": row['CVSS3 Base Score'],
                    "cvss3_temporal_score": row['CVSS3 Temporal Score'],
                    "cvss3_temporal_vector": row['CVSS3 Temporal Vector'],
                    "cvss3_vector": row['CVSS3 Vector'],
                    "system_type": row['System Type'],
                    "host_start": row['Host Start'],
                    "host_end": row['Host End']
                }


@click.command()
@click.option('--reindex', is_flag=True, default=False, help='Delete and recreate findings index')
def main(reindex):
    print('Making Elasticsearch Client...')
    try:
        client = Elasticsearch(
            hosts=['https://search-nessus-test-vpxqbl5ji7uabhbdn6wmahihly.us-east-2.es.amazonaws.com:443'],
            use_ssl=True,
            verify_certs=True
        )
    except (TransportError, SSLError, ConnectionError) as error:
        print("ElasticSearch client error: ", error)
        sys.exit(1)

    # If '--reindex' was passed, kill the findings index first and create a new one
    if reindex:
        print("Deleting Elasticsearch findings index first...")
        client.indices.delete(index='findings')
        print('Creating new Elasticsearch findings index...')
        create_index(client)

    # Make a list of all the CSV files in the reports directory
    csv_files = list()
    files = os.listdir('reports')
    for file in files:
        if file.endswith('.csv'):
            csv_files.append(file)

    # Index findings from every CSV file in the reports directory
    for csv_file in csv_files:
        print(f'Indexing findings in {csv_file}...')
        number_of_findings = get_total_rows(f'reports/{csv_file}')

        # If there are zero findings, skip to the next file
        if not number_of_findings:
            print(f'Skipped {csv_file} since it had zero findings...')
            continue

        # Bulk stream findings to Elasticsearch endpoint using our generator 'gendata()'
        successes = 0
        for ok, action in streaming_bulk(client=client, index='findings', actions=gendata(f'reports/{csv_file}')):
            successes += ok

        print(f'Indexed {successes}/{number_of_findings} findings from {csv_file}')


if __name__ == '__main__':
    main()
    print('Operation(s) completed')
