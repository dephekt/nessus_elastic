#!/usr/bin/env python3
import os
import sys

from tenable.io import TenableIO


def list_scans(tio: TenableIO) -> list:
    """Return a list of completed scan job metadata."""
    scans = tio.scans.list()
    complete_scans = list()

    for scan in scans:
        if scan['status'] == 'completed':
            complete_scans.append(scan)

    return complete_scans


def download_reports():
    """Download completed scan reports."""
    print('Creating Tenable.io API client...')
    tio_access = os.environ['TIO_ACCESS_KEY']
    tio_secret = os.environ['TIO_SECRET_KEY']
    tio = TenableIO(tio_access, tio_secret)

    print('Generating a list of completed vulnerability scans...')
    scans = list_scans(tio)

    if not scans:
        print('No completed vulnerability scans found, exiting!')
        sys.exit(1)

    if not os.path.exists('reports'):
        print('Creating a directory for our reports...')
        os.makedirs('reports')

    report_count = 0

    print(f'Preparing to download {len(scans)} reports...')
    for scan in scans:
        report_count += 1
        file = '.'.join(('_'.join((str(scan['id']), scan['name'])), 'csv'))
        with open(f'reports/{file}', 'wb') as reportobj:
            print(f'Downloading report for scan {scan["id"]} to {file}... ({report_count}/{len(scans)})')
            tio.scans.export(scan['id'], format='csv', fobj=reportobj)


if __name__ == '__main__':
    download_reports()
